mod output;
mod scraper;

pub use crate::scraper::extract_recipe;
pub use output::{output_recipe, OutputFormat};

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct Recipe {
    pub title: Option<String>,
    pub link: Option<String>,
    pub image: Option<String>,
    pub portions: Option<u32>,
    pub description: Option<String>,
    pub author: Author,
    pub ingredients: Vec<RecipeGroup<Ingredient>>,
    pub steps: Vec<RecipeGroup<String>>,
    pub language: Option<String>,
    pub source: Option<String>,
}

#[derive(Default, Debug, Deserialize, Serialize)]
pub struct Author {
    pub name: Option<String>,
    pub link: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RecipeGroup<T> {
    name: Option<String>,
    items: Vec<T>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Ingredient {
    /// Name of ingredient
    pub name: String,
    /// Amount of ingredient
    pub amount: Option<f32>,
    /// Measurement used in amount
    pub measurement: Option<String>,
}

/// Error for recipe-dl
#[derive(Debug)]
pub enum Error {
    NoScraperFound,
}
