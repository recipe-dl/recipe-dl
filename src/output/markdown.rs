use crate::Recipe;
use std::fmt::{self, Write};

pub fn to_markdown(recipe: &Recipe) -> Result<String, fmt::Error> {
    let mut markdown = String::new();
    if let Some(title) = &recipe.title {
        writeln!(markdown, "# {}", title)?;
    }

    if let (Some(name), Some(link)) = (&recipe.author.name, &recipe.author.link) {
        writeln!(markdown, "By *[{}]({})*\n", name, link)?;
    } else if let Some(name) = &recipe.author.name {
        writeln!(markdown, "By *{}*", name)?;
    } else if let Some(link) = &recipe.author.link {
        writeln!(markdown, "By {}", link)?;
    }

    if let Some(link) = &recipe.link {
        writeln!(markdown, "[*Source*]({})\n", link)?;
    }

    if let Some(description) = &recipe.description {
        writeln!(markdown, "**{}**\n", description)?;
    }

    if let Some(portions) = &recipe.portions {
        writeln!(markdown, "Portions: {}\n", portions)?;
    }

    writeln!(markdown, "## Ingredients")?;
    for group in &recipe.ingredients {
        for ingredient in &group.items {
            writeln!(markdown, " * {}", ingredient)?;
        }
    }

    writeln!(markdown, "\n##Steps:")?;
    for group in &recipe.steps {
        for (i, step) in group.items.iter().enumerate() {
            writeln!(markdown, " {}. {}", i + 1, step)?;
        }
    }
    Ok(markdown)
}
