use std::str::FromStr;

use crate::Recipe;

mod markdown;
pub mod text;

pub enum OutputFormat {
    Json,
    Markdown,
    Text,
}

/// Create string representation of recipe
pub fn output_recipe(recipe: &Recipe, output_format: &OutputFormat) -> String {
    match output_format {
        OutputFormat::Json => serde_json::to_string_pretty(&recipe).unwrap(),
        OutputFormat::Markdown => markdown::to_markdown(recipe).unwrap(),
        OutputFormat::Text => format!("{}", recipe),
    }
}

impl FromStr for OutputFormat {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "json" => Self::Json,
            "markdown" => Self::Markdown,
            "text" => Self::Text,
            x => return Err(
                format!(
                    "{} is not a valid output format. Valid formats are: json, markdown, and text",
                    x
                ))
        })
    }
}
