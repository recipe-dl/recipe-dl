use std::fmt;

use crate::{Author, Ingredient, Recipe};

impl fmt::Display for Recipe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(title) = &self.title {
            writeln!(f, "{}", title)?;
            writeln!(f, "{}", "=".repeat(title.len()))?;
        }

        writeln!(f, "Author: {}\n", self.author)?;

        if let Some(link) = &self.link {
            writeln!(f, "Source: {}\n", link)?;
        }

        if let Some(description) = &self.description {
            writeln!(f, "{}\n", description)?;
        }

        if let Some(portions) = &self.portions {
            writeln!(f, "Portions: {}\n", portions)?;
        }

        writeln!(f, "Ingredients:")?;
        for group in &self.ingredients {
            for ingredient in &group.items {
                writeln!(f, "\t{}", ingredient)?;
            }
        }

        writeln!(f, "\nSteps:")?;
        for group in &self.steps {
            for (i, step) in group.items.iter().enumerate() {
                writeln!(f, "\t{}. {}", i + 1, step)?;
            }
        }
        Ok(())
    }
}
impl fmt::Display for Author {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(name) = &self.name {
            write!(f, "{}", name)?;
        }
        if let Some(link) = &self.link {
            write!(f, " ({})", link)?;
        }
        Ok(())
    }
}

impl fmt::Display for Ingredient {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(amount) = &self.amount {
            write!(f, "{} ", amount)?;
        }
        if let Some(measurement) = &self.measurement {
            write!(f, "{} ", measurement)?;
        }
        write!(f, "{}", self.name)
    }
}
