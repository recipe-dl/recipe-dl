use super::{
    util::{scraper, scraper_from_template, sel, val},
    Scraper,
};

lazy_static::lazy_static! {
    pub static ref SCRAPERS: Vec<Scraper> = vec![
        scraper! {
            name: "allrecipes",
            url: r"allrecipes.com/recipe/\d+",
            title: sel!(".headline"),
            image: sel!(".lead-media .inner-container img", "src"),
            description: sel!(".recipe-summary p"),
            author_name: sel!(".author-name"),
            author_link: sel!(".author-name", "href"),
            portions: sel!(".recipe-adjust-servings__size-quantity"),
            ingredients: sel!(".ingredients-item-name"),
            steps: sel!(".instructions-section p"),
            language: val("English")
        },
        scraper! {
            name: "delish",
            url: r"delish.com/cooking/recipe-ideas/",
            title: sel!(".recipe-hed"),
            image: sel!(".content-lede-image-wrap img", "src"),
            ingredients: sel!(".ingredient-item"),
            steps: sel!(".direction-lists li"),
            language: val("English")
        },
        scraper_from_template! {
            tmpl: TASTY_RECIPES_TEMPLATE,
            name: "Gimme Some Oven",
            url: r"gimmesomeoven.com/[^/]+/$",
            author_name: val("Ali"),
            author_link: val("https://gimmesomeoven.com/about")
        },
        scraper_from_template! {
            tmpl: WPRM_TEMPLATE,
            name: "Madens Verden",
            url: r"madensverden.dk/[^/]+/$",
            title: sel!(".entry-title"),
            image: sel!("img.singlefeaturedimage", "src"),
            author_name: sel!(".author_name"),
            language: val("Danish")
        },
        scraper_from_template! {
            tmpl: WPRM_TEMPLATE,
            name: "My Korean Kitchen",
            url: r"mykoreankitchen.com/[^/]+/$",
            author_link: sel!("h4.gb-headline > a:nth-child(1)", "href"),
            image: sel!("img.first-featured-image", "src")

        },
        scraper_from_template! {
            tmpl: TASTY_RECIPES_TEMPLATE,
            name: "Pinch of Yum",
            url: r"pinchofyum.com/[^/]+$",
            portions: sel!("li.yield"),
            author_name: val("Lindsay"),
            author_link: val("https://pinchofyum.com/about")
        },
        scraper! {
            name: "Samvirke",
            url: r"samvirke.dk/opskrifter/",
            title: sel!(".article-header--title"),
            description: sel!(".article-header--summary"),
            image: sel!(".image--image img", "src"),
            ingredients: sel!(".recipe-full--ingredients tr"),
            steps: sel!(".how-to--description"),
            language: val("Danish")
        },
        scraper! {
            name: "Tasty",
            url: r"tasty.co/recipe/",
            title: sel!(".recipe-name"),
            image: sel!(".video-thumbnail img", "src"),
            author_name: sel!(".byline"),
            description: sel!(".description"),
            ingredients: sel!(".ingredient"),
            steps: sel!(".prep-steps li"),
            language: val("English")
        },
    ];
}
