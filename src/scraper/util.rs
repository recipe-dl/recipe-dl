use super::SelectionMethod;
use crate::scraper::Scraper;

/// Select elements based on regular expression
pub fn re(x: &str) -> regex::Regex {
    regex::Regex::new(x).unwrap()
}

pub fn selector(sel: &str, attribute: Option<String>) -> SelectionMethod {
    SelectionMethod::CssSelector {
        selector: scraper::Selector::parse(sel).unwrap(),
        attribute,
    }
}

/// Select elements based on css selector
macro_rules! sel {
    ($selector:expr) => {
        crate::scraper::util::selector($selector, None)
    };
    ($selector:expr, $attribute:expr) => {
        crate::scraper::util::selector($selector, Some($attribute.to_string()))
    };
}
pub(super) use sel;

/// Static value selection method
pub fn val(value: &str) -> SelectionMethod {
    SelectionMethod::Value(value.to_string())
}

/// Create `Scraper`
macro_rules! scraper {
    (name: $name:expr, url: $url:expr, $($x:ident: $value:expr),+) => {{
        let mut s = crate::scraper::Scraper {
            name: $name.to_string(),
            url: regex::Regex::new($url).unwrap(),
            test: Default::default(),
            title: Default::default(),
            image: Default::default(),
            ingredient_group_container: Default::default(),
            ingredient_group_name: Default::default(),
            ingredients: Default::default(),
            step_group_container: Default::default(),
            step_group_name: Default::default(),
            steps: Default::default(),
            author_name: Default::default(),
            author_link: Default::default(),
            portions: Default::default(),
            description: Default::default(),
            language: Default::default(),
            bad_characters: Default::default(),
        };
        $(
            s.$x = $value;
        )+
        s
    }}
}
lazy_static::lazy_static! {
    /// Wordpress Recipe Maker Template
    pub static ref WPRM_TEMPLATE: Scraper = scraper! {
        name: "WPRM_TEMPLATE".to_string(),
        url: "",
        test: sel!(".wprm-recipe"),
        title: sel!(".wprm-recipe-name"),
        ingredient_group_container: sel!(".wprm-recipe-ingredient-group"),
        ingredient_group_name: sel!(".wprm-recipe-group-name"),
        ingredients: sel!(".wprm-recipe-ingredient"),
        steps: sel!(".wprm-recipe-instruction-text"),
        author_name: sel!(".wprm-recipe-author"),
        portions: sel!(".wprm-recipe-servings"),
        language: val("English"),
        bad_characters: vec!['▢']
    };
    /// Tasty Recipes wordpress template
    pub static ref TASTY_RECIPES_TEMPLATE: Scraper = scraper! {
        name: "TASTY_RECIPE_TEMPLATE".to_string(),
        url: "",
        test: sel!(".tasty-recipes"),
        title: sel!(".tasty-recipes-title"),
        image: sel!(".tasty-recipes-image img", "src"),
        ingredients: sel!(".tasty-recipes-ingredients li"),
        steps: sel!(".tasty-recipes-instructions li"),
        author_name: sel!(".tasty-recipes-author-name"),
        portions: sel!(".tasty-recipes-yield span"),
        description: sel!(".tasty-recipes-description"),
        language: val("English")
    };
}

/// Create `Scraper`
macro_rules! scraper_from_template {
    (tmpl: $tmpl:ident, name: $name:expr, url: $url:expr, $($x:ident: $value:expr),+) => {{
        use crate::scraper::util::$tmpl;
        let mut s = $tmpl.clone();
        s.name = $name.to_string();
        s.url = regex::Regex::new($url).unwrap();
        $(
            s.$x = $value;
        )+
        s
    }}
}

pub(super) use scraper;
pub(super) use scraper_from_template;
