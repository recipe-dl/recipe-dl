use crate::{Author, Ingredient, Recipe, RecipeGroup};
use lazy_static::lazy_static;
use regex::Regex;
use scraper::{Html, Selector};

use std::collections::HashMap;

mod scrapers;
mod util;

#[derive(Debug, Clone)]
pub struct Scraper {
    /// Name of site
    name: String,
    /// Regex url has to match for scraper to apply
    url: Regex,
    /// Additional test
    test: SelectionMethod,
    /// Selection of title of recipe
    title: SelectionMethod,
    /// Selection of link to image
    image: SelectionMethod,
    /// Container for ingredient group
    ingredient_group_container: SelectionMethod,
    /// Name for group of ingredients
    ingredient_group_name: SelectionMethod,
    /// Selection of ingriedients
    ingredients: SelectionMethod,
    /// Container for ingredient group,
    step_group_container: SelectionMethod,
    /// Name of group for steps
    step_group_name: SelectionMethod,
    /// Selection of steps
    steps: SelectionMethod,
    /// Selection of author name
    author_name: SelectionMethod,
    /// Selection of link to author
    author_link: SelectionMethod,
    /// Selection of portions
    portions: SelectionMethod,
    /// Selection of description
    description: SelectionMethod,
    /// Selection of language
    language: SelectionMethod,
    /// Characters to remove from selections
    bad_characters: Vec<char>,
}

#[derive(Debug, PartialEq, Clone)]
pub enum SelectionMethod {
    None,
    CssSelector {
        selector: Selector,
        attribute: Option<String>,
    },
    Value(String),
}

impl Default for SelectionMethod {
    fn default() -> Self {
        Self::None
    }
}

/// Check if scraper matches conditions
fn scraper_match_conditions(scraper: &Scraper, url: &str, doc: &Html) -> bool {
    scraper.url.is_match(url)
        && (scraper.test == SelectionMethod::None
            || extract_value(&scraper.test, &doc.root_element()).is_some())
}

/// Find scraper that matches conditions
fn find_scraper<'a>(url: &'a str, doc: &Html) -> Option<&'a Scraper> {
    scrapers::SCRAPERS
        .iter()
        .find(|x| scraper_match_conditions(x, url, doc))
}

lazy_static! {
    // TODO: A more complete list
    static ref MEASUREMENTS: HashMap<&'static str, Option<&'static str>> = HashMap::from([
        ("centimeter", Some("cm")),
        ("cloves", None),
        ("cup", None),
        ("deciliter", Some("dl")),
        ("dl", None),
        ("dåser", None),
        ("fed", None),
        ("g", None),
        ("gram", Some("g")),
        ("kg", None),
        ("kilo", Some("kg")),
        ("l", None),
        ("lb", None),
        ("liter", Some("l")),
        ("oz", None),
        ("spiseskefulde", Some("spsk")),
        ("spsk", None),
        ("stk", None),
        ("teske", Some("tsk")),
        ("tablespoon", Some("tbsp.")),
        ("tablespoons", Some("tbsp.")),
        ("tbsp", None),
        ("teaspoon", Some("tsp.")),
        ("teaspoons", Some("tsp.")),
        ("tsk", None),
        ("tsp", None),
    ]);

}

/// Extract measurement and amount from string and create `Ingredient`
fn create_ingredient<T: ToString>(input: T) -> Ingredient {
    let input_string = input.to_string();
    let input_parts: Vec<&str> = input_string.split_whitespace().collect();

    let mut name_index = 0;
    let mut amount: Option<f32> = None;
    let mut measurement: Option<String> = None;

    if let Ok(parsed_amount) = input_parts[0].replace(',', ".").parse::<f32>() {
        name_index += 1;
        amount = Some(parsed_amount);
        if let Some((input_measurement, better_measurement)) =
            MEASUREMENTS.get_key_value(&input_parts[1])
        {
            name_index += 1;
            measurement = if let Some(better_measurement) = better_measurement {
                Some((*better_measurement).into())
            } else {
                Some((*input_measurement).into())
            }
        }
    }

    Ingredient {
        name: input_parts[name_index..].join(" "),
        amount,
        measurement,
    }
}

/// Extract ingredient or step groups from document
fn extract_groups<T>(
    doc: &Html,
    container: &SelectionMethod,
    name: &SelectionMethod,
    items: &SelectionMethod,
    parse: &dyn Fn(String) -> T,
) -> Vec<RecipeGroup<T>> {
    let extract_group = |elem| RecipeGroup {
        name: extract_value(&name, &elem),
        items: extract_values(&items, &elem)
            .into_iter()
            .map(|x| parse(x))
            .collect(),
    };
    match container {
        &SelectionMethod::None => {
            let elem = doc.root_element();
            vec![extract_group(elem)]
        }
        SelectionMethod::CssSelector {
            selector,
            attribute: _,
        } => doc.select(selector).map(extract_group).collect(),
        _ => unimplemented!(),
    }
}

/// Extract attribute from Element.
/// If attribute is not specified then text content will be extracted.
fn extract_attribute(elem: scraper::ElementRef, attribute: &Option<String>) -> Option<String> {
    match attribute {
        // Extract attribute
        Some(attr) => elem.value().attr(attr).map(String::from),
        // Extract text
        None => Some(elem.text().collect::<String>().trim().to_string()),
    }
}

/// Extract the first matching value from document
fn extract_value(selection: &SelectionMethod, doc: &scraper::ElementRef) -> Option<String> {
    match selection {
        SelectionMethod::CssSelector {
            selector,
            attribute,
        } => doc
            .select(selector)
            .next()
            .map(|x| extract_attribute(x, attribute))
            .flatten(),
        SelectionMethod::None => None,
        SelectionMethod::Value(x) => Some(x.clone()),
    }
}

/// Extract all matching values from document
fn extract_values(selection: &SelectionMethod, doc: &scraper::ElementRef) -> Vec<String> {
    match selection {
        SelectionMethod::CssSelector {
            selector,
            attribute,
        } => doc
            .select(selector)
            .filter_map(|elem| extract_attribute(elem, attribute))
            .collect(),
        SelectionMethod::None => Vec::new(),
        SelectionMethod::Value(x) => vec![x.clone()],
    }
}

/// Create recipe from scraper and document
fn scrape_site(scraper: &Scraper, url: &str, doc: &Html) -> Recipe {
    let root = doc.root_element();
    Recipe {
        title: extract_value(&scraper.title, &root),
        link: Some(url.to_string()),
        image: extract_value(&scraper.image, &root),
        description: extract_value(&scraper.description, &root),
        author: Author {
            name: extract_value(&scraper.author_name, &root),
            link: extract_value(&scraper.author_link, &root),
        },
        ingredients: extract_groups(
            &doc,
            &scraper.ingredient_group_container,
            &scraper.ingredient_group_name,
            &scraper.ingredients,
            &|x| create_ingredient(x.replace(&scraper.bad_characters[..], "")),
        ),
        steps: extract_groups(
            &doc,
            &scraper.step_group_container,
            &scraper.step_group_name,
            &scraper.steps,
            &|x| x,
        ),
        portions: extract_value(&scraper.portions, &root)
            .map(|x| x.parse::<u32>().ok())
            .flatten(),
        language: extract_value(&scraper.language, &root),
        source: Some(scraper.name.clone()),
        ..Default::default()
    }
}

pub fn extract_recipe(url: &str, page: &str) -> Result<Recipe, super::Error> {
    let doc = scraper::Html::parse_document(page);
    let scraper = find_scraper(url, &doc).ok_or(super::Error::NoScraperFound)?;
    let recipe = scrape_site(scraper, url, &doc);
    Ok(recipe)
}

#[cfg(test)]
mod test {

    #[test]
    fn scrape_site() {
        let page = std::fs::read_to_string("./tests/scraper_data/madensverden.html").unwrap();
        let scraper = super::scrapers::SCRAPERS
            .iter()
            .find(|x| &x.name == "Madens Verden")
            .unwrap();
        let doc = scraper::Html::parse_document(&page);
        let recipe = super::scrape_site(
            &scraper,
            "https://madensverden.dk/laks-med-rodfrugter/",
            &doc,
        );
        assert_eq!(
            &recipe.title.unwrap(),
            "Laks med rodfrugter i ovn - sund og nem aftensmad"
        );
        assert_eq!(recipe.ingredients[0].items.len(), 11);
        assert_eq!(recipe.steps[0].items.len(), 7);
    }
}
