use recipedl::OutputFormat;
use structopt::StructOpt;

/// CLI for downloading recipes
#[derive(StructOpt)]
struct Arguments {
    /// Link to recipe
    link: String,
    /// Output format
    #[structopt(short, long, default_value = "json")]
    output_format: OutputFormat,
    /// Log level
    #[structopt(short, long, default_value = "warn")]
    log_level: log::LevelFilter,
}

/// Download page
async fn get_page(link: &str) -> Result<String, reqwest::Error> {
    let client = reqwest::Client::new();
    client.get(link).send().await?.text().await
}

#[tokio::main]
async fn main() {
    let args = Arguments::from_args();
    initialize_fern(&args);
    let page = get_page(&args.link).await.expect("Could not download page");
    let recipe = recipedl::extract_recipe(&args.link, &page).expect("Could not extract recipe");
    println!("{}", recipedl::output_recipe(&recipe, &args.output_format));
}

fn initialize_fern(args: &Arguments) {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}] {}",
                record.target(),
                record.level(),
                message
            ))
        })
        .level(args.log_level)
        .chain(std::io::stderr())
        .apply()
        .expect("Fern failed to initialize");
}
